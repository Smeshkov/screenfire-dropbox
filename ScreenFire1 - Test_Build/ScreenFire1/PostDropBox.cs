﻿using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using Spring.IO;
using Spring.Social.OAuth1;
using Spring.Social.Dropbox.Api;
using Spring.Social.Dropbox.Connect;

namespace ScreenFire1
{
    class PostDropBox 
    {
        private const string DropboxAppKey = "5owyw16z3xzwvfr";
        private const string DropboxAppSecret = "xna8bx82n75tqhj";
        //DropboxServiceProvider dropboxServiceProvider = new DropboxServiceProvider(DropboxAppKey, DropboxAppSecret, AccessLevel.Full);
        public static bool CheckAccess ()
        {
            DropboxServiceProvider dropboxServiceProvider = new DropboxServiceProvider(DropboxAppKey, DropboxAppSecret, AccessLevel.Full);
            IDropbox dropbox = dropboxServiceProvider.GetApi(Properties.Settings.Default.UserKey, Properties.Settings.Default.UserSecret);
            return true;
        }

        public static void Login()
        {
            DropboxServiceProvider dropboxServiceProvider = new DropboxServiceProvider(DropboxAppKey, DropboxAppSecret, AccessLevel.Full);
            OAuthToken oauthToken = dropboxServiceProvider.OAuthOperations.FetchRequestTokenAsync(null, null).Result;
            OAuth1Parameters parameters = new OAuth1Parameters();
            string authenticateUrl = dropboxServiceProvider.OAuthOperations.BuildAuthorizeUrl(oauthToken.Value, parameters);
            Process.Start(authenticateUrl);
        }

        public static string Get_Access_Token ()
        {
            try 
            {
            DropboxServiceProvider dropboxServiceProvider = new DropboxServiceProvider(DropboxAppKey, DropboxAppSecret, AccessLevel.Full);
            OAuthToken oauthToken = dropboxServiceProvider.OAuthOperations.FetchRequestTokenAsync(null, null).Result;
            OAuth1Parameters parameters = new OAuth1Parameters();
            string authenticateUrl = dropboxServiceProvider.OAuthOperations.BuildAuthorizeUrl(oauthToken.Value, parameters);
            Process.Start(authenticateUrl);
            AuthorizedRequestToken requestToken = new AuthorizedRequestToken(oauthToken, null);
            bool Accepted = false;
            while (!Accepted)
            {
                try
                {
                    OAuthToken oauthAccessToken = dropboxServiceProvider.OAuthOperations.ExchangeForAccessTokenAsync(requestToken, null).Result;
                    Properties.Settings.Default.UserKey = oauthAccessToken.Value;
                    Properties.Settings.Default.UserSecret = oauthAccessToken.Secret;
                    Properties.Settings.Default.Save();
                    IDropbox dropbox = dropboxServiceProvider.GetApi(Properties.Settings.Default.UserKey, Properties.Settings.Default.UserSecret);
                    DropboxProfile profile = dropbox.GetUserProfileAsync().Result;
                    Accepted = true;
                    return profile.DisplayName;
                }
                catch (AggregateException)
                {
                    continue;
                }
                
            }
            return "gg";
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                    {
                        if (ex is DropboxApiException)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return true;
                        }
                        return false;
                    });
                return "error";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "error";
            }

            /*AuthorizedRequestToken requestToken = new AuthorizedRequestToken(oauthToken, null);
            //OAuthToken oauthAccessToken = dropboxServiceProvider.OAuthOperations.ExchangeForAccessTokenAsync(requestToken, null).Result;
            Properties.Settings.Default.UserKey = oauthAccessToken.Value;
            Properties.Settings.Default.UserSecret = oauthAccessToken.Secret;
            IDropbox dropbox = dropboxServiceProvider.GetApi(Properties.Settings.Default.UserKey, Properties.Settings.Default.UserSecret);
            DropboxProfile profile = dropbox.GetUserProfileAsync().Result;
            return profile.DisplayName +" "+ Properties.Settings.Default.UserKey +" "+ Properties.Settings.Default.UserSecret;*/
        }
        public static string PostToDropBox (string filename)
        {
            try {
            var res = new FileResource(filename);
            DropboxServiceProvider dropboxServiceProvider = new DropboxServiceProvider(DropboxAppKey, DropboxAppSecret, AccessLevel.Full);
            OAuthToken oauthToken = dropboxServiceProvider.OAuthOperations.FetchRequestTokenAsync(null, null).Result;
            OAuth1Parameters parameters = new OAuth1Parameters();
            IDropbox dropbox = dropboxServiceProvider.GetApi(Properties.Settings.Default.UserKey, Properties.Settings.Default.UserSecret);
            DropboxProfile profile = dropbox.GetUserProfileAsync().Result;
            Entry uploadFileEntry = dropbox.UploadFileAsync(res, "/ScreenFire/"+Path.GetFileName(filename), true, null, CancellationToken.None).Result;
            DropboxLink shareableLink = dropbox.GetShareableLinkAsync("ScreenFire/" + Path.GetFileName(filename)).Result;
            File.Delete(filename);
            return Convert.ToString(shareableLink.Url);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "error";
            }
        }


    }
}
