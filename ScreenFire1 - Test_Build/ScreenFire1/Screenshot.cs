﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScreenFire1
{
    /// <summary>
    /// Class which is taking and saving screenshots
    /// </summary>
    class Screenshot
    {
        const string FILENAME_FORMAT = "{0:dd-MM-yyyy HH-mm-ss}";

        Bitmap _bitmap;
        DateTime _timestamp;
        string _fileName;

        /// <summary>
        /// Can be used to access the taken screenshot (for example, for editing)
        /// </summary>
        public Bitmap Bitmap
        {
            get { return _bitmap; }
            set { }
        }

        /// <summary>
        /// The filename of saved picture
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set { }
        }


        public Screenshot()
        {

        }

        public void TakeAndSave()
        {
            _bitmap = TakeScreenShot(Screen.PrimaryScreen);
            _timestamp = DateTime.Now;
            SaveScreenShot();
        }

        private void SaveScreenShot()
        {
            string filename = Form1.GetExecutableDirectory() + String.Format(FILENAME_FORMAT, _timestamp) + ".jpg";
            if (!System.IO.Directory.Exists(Form1.GetExecutableDirectory()))
                System.IO.Directory.CreateDirectory(Form1.GetExecutableDirectory());
            _bitmap.Save(filename, ImageFormat.Jpeg);
            _fileName = filename;
        }

        private Bitmap TakeScreenShot(Screen currentScreen)
        {
            Bitmap bmpScreenShot = new Bitmap(currentScreen.Bounds.Width,
                                              currentScreen.Bounds.Height,
                                              PixelFormat.Format32bppArgb);

            Graphics gScreenShot = Graphics.FromImage(bmpScreenShot);

            gScreenShot.CopyFromScreen(currentScreen.Bounds.X,
                                       currentScreen.Bounds.Y,
                                       0, 0,
                                       currentScreen.Bounds.Size,
                                       CopyPixelOperation.SourceCopy);
            return bmpScreenShot;
        }
    }
}
