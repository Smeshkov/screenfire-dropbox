﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;


namespace ScreenFire1
{
    public partial class Form1 : Form
    {
        private Hook _hook;
        private string lastScreenshotUri = "";

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        public Form1()
        {
            InitializeComponent();
            keybd_event(0x90, 0x45, 0x1, (UIntPtr)0);

            // 0x90 клавиша NumLock
            _hook = new Hook(0x90); //Передаем код клавиши на которую ставим хук, тут это CapsLock

            _hook.KeyPressed += new KeyPressEventHandler(_hook_KeyPressed);
            _hook.SetHook();
        }

        void _hook_KeyPressed(object sender, KeyPressEventArgs e) //Событие нажатия клавиш
        {
            ProcessScreenshot();
        }

        public void ProcessScreenshot()
        {
            Screenshot screenshot = new Screenshot();
            screenshot.TakeAndSave();
            ShowNotificationBaloon("Uploading screenshot...");
            //label1.Text = (Path.GetFileName(screenshot.FileName));

            lastScreenshotUri = PostDropBox.PostToDropBox(screenshot.FileName);
            Clipboard.SetText(lastScreenshotUri);
            //Clipboard.SetText(lastScreenshotUri);
            textBox1.Text = (lastScreenshotUri);
            ShowNotificationBaloon("Done");
            File.Delete(screenshot.FileName);
            //Clipboard.SetText(lastScreenshotUri);
            //Clipboard.SetText(lastScreenshotUri);
            //label2.Text = Clipboard.GetText();          
        }


        private void ShowNotificationBaloon(string message, string title = "ScreenFire")
        {
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.BalloonTipTitle = title;
            notifyIcon1.BalloonTipText = message;
            notifyIcon1.ShowBalloonTip(10000);
        }


        public static string GetExecutableDirectory()
        {
            return Path.GetDirectoryName(Application.ExecutablePath) + @"\";
        }

        private void Form1_Deactivate_1(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            } 
        }

       /* public void Buffer(string link)
        {
            Clipboard.SetText(link);
        }*/
        private void notifyIcon1_Click_1(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            } 
        }

        public void Activate_Form ()
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void menuItem1_Click(object Sender, EventArgs e)
        {
            // Close the form, which closes the application.
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            //label2.Text = Properties.Settings.Default.UserKey;
           // MessageBox.Show("ERROR", "ERROR title", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = PostDropBox.Get_Access_Token();
            this.Activate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text= PostDropBox.Get_Access_Token();
        }




    }
}
